﻿namespace DataLayer.Models
{
    public class City
    {
        public int City_Id { get; set; }
        public int Ppt { get; set; }
        public string City_Name { get; set; }

        public City() { }
    }
}