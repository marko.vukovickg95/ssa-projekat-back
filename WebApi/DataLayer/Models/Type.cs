﻿namespace DataLayer.Models
{
    public class Type
    {
        public int Type_Id { get; set; }
        public string Type_Name { get; set; }

        public Type() { }
    }
}